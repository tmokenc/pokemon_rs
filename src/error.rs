use std::fmt;

#[derive(Debug)]
pub enum Error {
    InvalidInput,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Error::*;

        match self {
            InvalidInput => write!(f, "Invalid input value"),
        }
    }
}

impl std::error::Error for Error {}
