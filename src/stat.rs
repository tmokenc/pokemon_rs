use std::fmt;
use std::str::FromStr;
use crate::nature::Nature;

#[derive(Debug, Clone, Copy, PartialEq)]
/// Represent a signle stat
pub enum Stat {
    Hp,
    Attack,
    Defense,
    SpecialAttack,
    SpecialDefense,
    Speed,
}

#[derive(Debug, Clone, PartialEq)]
/// Represent full stats
/// This can be use as base stats or EV/IV stats
pub struct Stats {
    pub hp: u8,
    pub attack: u8,
    pub defense: u8,
    pub sp_attack: u8,
    pub sp_defense: u8,
    pub speed: u8,
}

#[derive(Default, Debug, Clone, PartialEq)]
/// The calculated stats
/// For use as a cache to not having calculate stats every time it is needed
pub struct CalculatedStats {
    hp: u16,
    atk: u16,
    def: u16,
    spatk: u16,
    spdef: u16,
    speed: u16,
}

#[derive(Debug, Clone, PartialEq)]
/// Everything to calculate a stat
pub struct StatBased {
    pub base: u8,
    pub level: u8,
    pub iv: u8,
    pub ev: u8,
    pub nature: Nature,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Flavor {
    Spicy,
    Sour,
    Sweet,
    Dry,
    Bitter,
}

impl Stat {
    fn cal_basic(stats: &StatBased) -> u16 {
        let double_base: u16 = 2 * stats.base as u16;
        let ev_rounded: u16 = (stats.ev / 4) as u16;
        let tmp: u16 = (double_base + stats.iv as u16 + ev_rounded) * stats.level as u16;
        tmp / 100
    }

    fn calculate_hp(stats: StatBased) -> u16 {
        Self::cal_basic(&stats) + stats.level as u16 + 10
    }
    
    pub fn calculate(self, stats: StatBased) -> u16 {
        if let Self::Hp = self {
            return Self::calculate_hp(stats)
        }
        
        let mut stat = (Self::cal_basic(&stats) + 5) as f32;
        let nature_change = match stats.nature.stats_change() {
            Some((v, _)) if v == self => 1.1,
            Some((_, v)) if v == self => 0.9,
            _ => 1.0
        };
        
        stat *= nature_change;
        stat.floor() as u16
    }
}

impl CalculatedStats {
    #[inline]
    pub fn stat(&self, stat: Stat) -> u16 {
        match stat {
            Stat::Hp => self.hp,
            Stat::Attack => self.atk,
            Stat::Defense => self.def,
            Stat::SpecialAttack => self.spatk,
            Stat::SpecialDefense => self.spdef,
            Stat::Speed => self.speed,
        }
    }
    
    #[inline]
    pub fn update(&mut self, stat: Stat, value: u16) {
        match stat {
            Stat::Hp => self.hp = value,
            Stat::Attack => self.atk = value,
            Stat::Defense => self.def = value,
            Stat::SpecialAttack => self.spatk = value,
            Stat::SpecialDefense => self.spdef = value,
            Stat::Speed => self.speed = value,
        }
    }
}

impl Default for StatBased {
    fn default() -> Self {
        Self {
            base: 1,
            level: 50,
            iv: 31,
            ev: 0,
            nature: Nature::Docile,
        }
    }
}

impl StatBased {
    /// Check if the stats is valid, the level can be higher than 100
    pub fn is_valid(&self) -> bool {
        self.base > 0
        && self.level > 0
        && self.iv <= 31
    }

    /// Check if the stats is legit in pokemon core games
    pub fn is_legit(&self) -> bool {
        self.base > 0
        && self.level > 0 
        && self.level <= 100
        && self.iv <= 31
    }
}

impl FromStr for Stat {
    type Err = crate::Error;

    fn from_str(n: &str) -> Result<Self, Self::Err> {
        use Stat::*;

        match n.to_lowercase().as_str() {
            "hp" => Ok(Hp),
            "attack" | "atk" => Ok(Attack),
            "defense" | "defend" | "def" => Ok(Defense),
            "specialattack" | "spattack" | "sp.attack" | "spatk" | "sp.atk" => Ok(SpecialAttack),
            "specialdefense" | "spdefense" | "sp.defense" | "sp.defend" | "spdef" | "sp.def" => {
                Ok(SpecialDefense)
            }
            "speed" | "spd" | "spe" => Ok(Speed),
            _ => Err(crate::Error::InvalidInput),
        }
    }
}

impl fmt::Display for Stat {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Stat::*;

        match self {
            Hp => write!(f, "HP"),
            Attack => write!(f, "Attack"),
            Defense => write!(f, "Defense"),
            SpecialAttack => write!(f, "Sp.Attack"),
            SpecialDefense => write!(f, "Sp.Defense"),
            Speed => write!(f, "Speed"),
        }
    }
}


impl FromStr for Flavor {
    type Err = crate::Error;

    fn from_str(n: &str) -> Result<Self, Self::Err> {
        use Flavor::*;

        match n.to_lowercase().as_str() {
            "spicy" => Ok(Spicy),
            "sour" => Ok(Sour),
            "sweet" => Ok(Sweet),
            "dry" => Ok(Dry),
            "bitter" => Ok(Bitter),
            _ => Err(crate::Error::InvalidInput),
        }
    }
}

impl fmt::Display for Flavor {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Flavor::*;

        let word = match self {
            Spicy => "Spicy",
            Sour => "Sour",
            Sweet => "Sweet",
            Dry => "Dry",
            Bitter => "Bitter",
        };
        
        write!(f, "{}", word)
    }
}

pub struct StatsIter {
    stats: Stats,
    current: u8,
}

impl IntoIterator for Stats {
    type Item = (Stat, u8);
    type IntoIter = StatsIter;
    
    fn into_iter(self) -> Self::IntoIter {
        StatsIter {
            stats: self,
            current: 0,
        }
    }
}

impl Iterator for StatsIter {
    type Item = (Stat, u8);
    
    fn next(&mut self) -> Option<Self::Item> {
        self.current += 1;
        
        match self.current {
            1 => Some((Stat::Hp, self.stats.hp)),
            2 => Some((Stat::Attack, self.stats.attack)),
            3 => Some((Stat::Defense, self.stats.defense)),
            4 => Some((Stat::SpecialAttack, self.stats.sp_attack)),
            5 => Some((Stat::SpecialDefense, self.stats.sp_defense)),
            6 => Some((Stat::Speed, self.stats.speed)),
            _ => None,
        }
    }
}

pub fn calculate_stats(
    level: u8,
    base: Stats, 
    iv: Stats, 
    ev: Stats,
    nature: Nature
) -> CalculatedStats {
    let mut result = CalculatedStats::default();
    let mut iv = iv.into_iter().map(|v| v.1);
    let mut ev = ev.into_iter().map(|v| v.1);
    
    for (stat, value) in base {
        let based = StatBased {
            level,
            nature,
            base: value,
            iv: iv.next().unwrap(),
            ev: ev.next().unwrap(),
        };
        
        result.update(stat, stat.calculate(based));
    }
    
    result
}