pub mod error;
pub mod nature;
pub mod stat;
pub mod types;

pub use error::Error;
pub use nature::Nature;
pub use stat::{Flavor, Stat, Stats, CalculatedStats};
pub use types::Type;

pub enum PossibleGender {
    Male,
    Female,
    MaleFemale,
    Genderless,
}

pub enum Gender {
    Male,
    Female,
    Genderless,
}

pub struct Pokemon {
    persional_id: u32,
    level: u8,
    nature: Nature,
    happiness: u8,
    iv: Stats,
    ev: Stats,
    stats: CalculatedStats,
    kind: Box<dyn PokemonKind>,
    ability: String,
    gender: Gender,
    moveset: [Option<String>; 4],
}

pub trait PokemonKind {
    fn name(&self) -> &str;
    fn base_stats(&self) -> Stats;
    fn types(&self) -> (Type, Option<Type>);
    fn gender(&self) -> PossibleGender;
    fn abilities(&self) -> &[&str];
    fn moves(&self) -> &[&str];
    
    fn can_learn(&self, move_to_learn: &str) -> bool {
        self.moves().contains(&move_to_learn)
    }
}
