use core::convert::TryFrom;
use core::str::FromStr;
use std::fmt;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Type {
    Normal,
    Fighting,
    Flying,
    Poison,
    Ground,
    Rock,
    Bug,
    Ghost,
    Steel,
    Fire,
    Water,
    Grass,
    Electric,
    Psychic,
    Ice,
    Dragon,
    Dark,
    Fairy,
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Type::*;

        let data = match *self {
            Normal => "normal",
            Fighting => "fighting",
            Flying => "flying",
            Poison => "poison",
            Ground => "ground",
            Rock => "rock",
            Bug => "bug",
            Ghost => "ghost",
            Steel => "steel",
            Fire => "fire",
            Water => "water",
            Grass => "grass",
            Electric => "electric",
            Psychic => "psychic",
            Ice => "ice",
            Dragon => "dragon",
            Dark => "dark",
            Fairy => "fairy",
        };

        write!(f, "{}", data)
    }
}

#[derive(Debug, Clone)]
pub struct ParseTypeError(String);

impl std::error::Error for ParseTypeError {}

impl fmt::Display for ParseTypeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Cannot parse the pokemon type of `{}`", &self.0)
    }
}

impl FromStr for Type {
    type Err = ParseTypeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use Type::*;

        let data = match s.to_lowercase().as_ref() {
            "normal" => Normal,
            "fighting" => Fighting,
            "flying" => Flying,
            "poison" => Poison,
            "ground" => Ground,
            "rock" => Rock,
            "bug" => Bug,
            "ghost" => Ghost,
            "steel" => Steel,
            "fire" => Fire,
            "water" => Water,
            "grass" => Grass,
            "electric" => Electric,
            "psychic" => Psychic,
            "ice" => Ice,
            "dragon" => Dragon,
            "dark" => Dark,
            "fairy" => Fairy,
            _ => return Err(ParseTypeError(s.to_owned())),
        };

        Ok(data)
    }
}

impl TryFrom<u8> for Type {
    type Error = crate::Error;

    fn try_from(n: u8) -> Result<Self, Self::Error> {
        use Type::*;

        match n {
            0 => Ok(Normal),
            1 => Ok(Fighting),
            2 => Ok(Flying),
            3 => Ok(Poison),
            4 => Ok(Ground),
            5 => Ok(Rock),
            6 => Ok(Bug),
            7 => Ok(Ghost),
            8 => Ok(Steel),
            9 => Ok(Fire),
            10 => Ok(Water),
            11 => Ok(Grass),
            12 => Ok(Electric),
            13 => Ok(Psychic),
            14 => Ok(Ice),
            15 => Ok(Dragon),
            16 => Ok(Dark),
            17 => Ok(Fairy),
            _ => Err(crate::Error::InvalidInput),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum TypeEffective {
    SuperEffective,
    Neutral,
    NotEffective,
    Immune,
}

impl From<TypeEffective> for f32 {
    fn from(t: TypeEffective) -> f32 {
        use TypeEffective::*;

        match t {
            SuperEffective => 2.0,
            Neutral => 1.0,
            NotEffective => 0.5,
            Immune => 0.0,
        }
    }
}

impl TypeEffective {
    #[inline]
    pub fn damaga_modify(&self) -> f32 {
        (*self).into()
    }
}

#[derive(Default)]
pub struct TypeIter {
    index: u8,
}

impl Iterator for TypeIter {
    type Item = Type;

    fn next(&mut self) -> Option<Self::Item> {
        let item = Type::try_from(self.index).ok();
        self.index += 1;
        item
    }
}

impl Type {
    #[inline]
    pub fn iter() -> TypeIter {
        TypeIter::default()
    }
    /// Defensive side
    pub fn weaknesses(&self) -> Vec<Type> {
        use Type::*;
        match *self {
            Normal => vec![Fighting],
            Fighting => vec![Flying, Psychic, Fairy],
            Flying => vec![Rock, Electric, Ice],
            Poison => vec![Ground, Psychic],
            Ground => vec![Water, Grass, Ice],
            Rock => vec![Fighting, Ground, Steel, Water, Grass],
            Bug => vec![Flying, Rock, Fire],
            Ghost => vec![Ghost, Dark],
            Steel => vec![Fighting, Ground, Fire],
            Fire => vec![Ground, Rock, Water],
            Water => vec![Grass, Electric],
            Grass => vec![Flying, Poison, Bug, Fire, Ice],
            Electric => vec![Ground],
            Psychic => vec![Bug, Ghost, Dark],
            Ice => vec![Fighting, Rock, Steel, Fire],
            Dragon => vec![Ice, Dragon, Fairy],
            Dark => vec![Fighting, Bug, Fairy],
            Fairy => vec![Steel, Poison],
        }
    }

    /// Defensive side
    pub fn resistances(&self) -> Vec<Type> {
        use Type::*;
        match *self {
            Normal => Vec::new(),
            Fighting => vec![Rock, Bug, Dark],
            Flying => vec![Fighting, Bug, Grass],
            Poison => vec![Fighting, Poison, Grass],
            Ground => vec![Poison, Rock],
            Rock => vec![Normal, Flying, Poison, Fire],
            Bug => vec![Fighting, Ground, Grass],
            Ghost => vec![Poison, Bug],
            Steel => vec![
                Normal, Flying, Rock, Bug, Steel, Grass, Psychic, Ice, Dragon, Fairy,
            ],
            Fire => vec![Bug, Steel, Fire, Grass, Ice, Fairy],
            Water => vec![Steel, Fire, Water, Ice],
            Grass => vec![Ground, Water, Grass, Electric],
            Electric => vec![Flying, Steel, Electric],
            Psychic => vec![Fighting, Psychic],
            Ice => vec![Ice],
            Dragon => vec![Fire, Water, Grass, Electric],
            Dark => vec![Ghost, Dark],
            Fairy => vec![Fighting, Bug, Dark],
        }
    }

    ///  Defensive side
    pub fn immune(&self) -> Vec<Type> {
        use Type::*;
        match *self {
            Ghost => vec![Normal, Fighting],
            Normal => vec![Ghost],
            Fairy => vec![Dragon],
            Dark => vec![Psychic],
            Steel => vec![Poison],
            Ground => vec![Electric],
            Flying => vec![Ground],
            _ => Vec::new(),
        }
    }

    /// Offensive power
    pub fn strong_to(&self) -> Vec<Type> {
        use Type::*;
        match *self {
            Normal => vec![],
            Fighting => vec![Normal, Rock, Steel, Ice, Dark],
            Flying => vec![Fighting, Bug, Grass],
            Poison => vec![Grass, Fairy],
            Ground => vec![Poison, Rock, Steel, Fire, Electric],
            Rock => vec![Flying, Bug, Fire, Ice],
            Bug => vec![Grass, Psychic, Dark],
            Ghost => vec![Ghost, Psychic],
            Steel => vec![Rock, Ice, Fairy],
            Fire => vec![Bug, Steel, Grass, Ice],
            Water => vec![Ground, Rock, Fire],
            Grass => vec![Ground, Rock, Water],
            Electric => vec![Flying, Water],
            Psychic => vec![Fighting, Poison],
            Ice => vec![Flying, Ground, Grass, Dragon],
            Dragon => vec![Dragon],
            Dark => vec![Ghost, Psychic],
            Fairy => vec![Fighting, Dragon, Dark],
        }
    }

    /// Offensive power
    pub fn weak_to(&self) -> Vec<Type> {
        use Type::*;
        match *self {
            Normal => vec![Rock, Steel],
            Fighting => vec![Flying, Poison, Bug, Psychic, Fairy],
            Flying => vec![Rock, Steel, Electric],
            Poison => vec![Poison, Ground, Rock, Ghost],
            Ground => vec![Bug, Grass],
            Rock => vec![Fighting, Ground, Steel],
            Bug => vec![Fighting, Flying, Poison, Ghost, Steel, Fire, Fairy],
            Ghost => vec![Dark],
            Steel => vec![Steel, Water, Electric],
            Fire => vec![Rock, Fire, Water, Dragon],
            Water => vec![Water, Grass, Dragon],
            Grass => vec![Flying, Poison, Bug, Steel, Fire, Grass, Dragon],
            Electric => vec![Grass, Electric, Dragon],
            Psychic => vec![Steel, Psychic],
            Ice => vec![Steel, Fire, Water, Ice],
            Dragon => vec![Steel],
            Dark => vec![Fighting, Dark, Fairy],
            Fairy => vec![Poison, Steel, Fire],
        }
    }

    /// Offensive power
    pub fn no_damage_to(&self) -> Vec<Type> {
        use Type::*;
        match *self {
            Normal => vec![Ghost],
            Fighting => vec![Ghost],
            Ghost => vec![Normal],
            Psychic => vec![Dark],
            Dragon => vec![Fairy],
            Poison => vec![Steel],
            Electric => vec![Ground],
            Ground => vec![Flying],
            _ => Vec::new(),
        }
    }

    pub fn effective(&self, other: Self) -> TypeEffective {
        use Type::*;
        use TypeEffective::*;

        match (*self, other) {
            (Fighting, Normal | Rock | Steel | Ice | Dark)
            | (Flying, Fighting | Bug | Grass)
            | (Poison, Grass | Fairy)
            | (Ground, Poison | Rock | Steel | Fire | Electric)
            | (Rock, Flying | Bug | Fire | Ice)
            | (Bug, Grass | Psychic | Dark)
            | (Ghost, Ghost | Psychic)
            | (Steel, Rock | Ice | Fairy)
            | (Fire, Bug | Grass | Steel | Ice)
            | (Water, Ground | Rock | Fire)
            | (Grass, Ground | Rock | Water)
            | (Electric, Flying | Water)
            | (Psychic, Fighting | Poison)
            | (Ice, Flying | Ground | Grass | Dragon)
            | (Dragon, Dragon)
            | (Dark, Ghost | Psychic)
            | (Fairy, Fighting | Dragon | Dark) => SuperEffective,

            (Normal, Rock | Steel)
            | (Fighting, Poison | Bug | Psychic | Fairy | Flying)
            | (Flying, Rock | Steel | Electric)
            | (Poison, Poison | Ground | Rock | Ghost)
            | (Ground, Bug | Grass)
            | (Rock, Fighting | Ground | Steel)
            | (Bug, Fighting | Flying | Poison | Ghost | Steel | Fire | Fairy)
            | (Ghost, Dark)
            | (Steel, Steel | Fire | Water | Electric)
            | (Fire, Rock | Fire | Water | Dragon)
            | (Water, Water | Grass | Dragon)
            | (Grass, Flying | Poison | Bug | Steel | Fire | Grass | Dragon)
            | (Electric, Grass | Electric | Dragon)
            | (Psychic, Steel | Psychic)
            | (Ice, Steel | Fire | Water | Ice)
            | (Dragon, Steel)
            | (Dark, Fighting | Dark | Fairy)
            | (Fairy, Poison | Steel | Fire) => NotEffective,

            (Normal, Ghost)
            | (Ghost, Normal)
            | (Fighting, Ghost)
            | (Ground, Flying)
            | (Electric, Ground)
            | (Psychic, Dark)
            | (Dragon, Fairy)
            | (Poison, Steel) => Immune,

            _ => Neutral,
        }
    }
}

#[cfg(test)]
mod test {
    use super::{Type, TypeEffective};

    #[test]
    fn weak_to() {
        for t in Type::iter() {
            for weakness in t.weaknesses() {
                println!("{} => {}", t, weakness);
                assert_eq!(TypeEffective::SuperEffective, weakness.effective(t))
            }
        }
    }

    #[test]
    fn resistances() {
        for t in Type::iter() {
            for resistance in t.resistances() {
                println!("{} => {}", t, resistance);
                assert_eq!(TypeEffective::NotEffective, resistance.effective(t))
            }
        }
    }

    #[test]
    fn strong_against() {
        for t in Type::iter() {
            for strong in t.strong_to() {
                println!("{} => {}", t, strong);
                assert_eq!(TypeEffective::SuperEffective, t.effective(strong))
            }
        }
    }

    #[test]
    fn weak_against() {
        for t in Type::iter() {
            for weak in t.weak_to() {
                println!("{} => {}", t, weak);
                assert_eq!(TypeEffective::NotEffective, t.effective(weak))
            }
        }
    }
}
