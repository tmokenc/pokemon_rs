use crate::error::Error;
use crate::stat::{Flavor, Stat};
use std::convert::TryFrom;
use std::fmt;
use std::str::FromStr;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Nature {
    Hardy,
    Lonely,
    Brave,
    Adamant,
    Naughty,
    Bold,
    Docile,
    Relaxed,
    Impish,
    Lax,
    Timid,
    Hasty,
    Serious,
    Jolly,
    Naive,
    Modest,
    Mild,
    Quiet,
    Bashful,
    Rash,
    Calm,
    Gentle,
    Sassy,
    Careful,
    Quirky,
}

impl FromStr for Nature {
    type Err = crate::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use Nature::*;

        match s.to_lowercase().as_str() {
            "hardy" => Ok(Hardy),
            "lonely" => Ok(Lonely),
            "brave" => Ok(Brave),
            "adamant" => Ok(Adamant),
            "naughty" => Ok(Naughty),
            "bold" => Ok(Bold),
            "docile" => Ok(Docile),
            "relaxed" => Ok(Relaxed),
            "impish" => Ok(Impish),
            "lax" => Ok(Lax),
            "timid" => Ok(Timid),
            "hasty" => Ok(Hasty),
            "serious" => Ok(Serious),
            "jolly" => Ok(Jolly),
            "naive" => Ok(Naive),
            "modest" => Ok(Modest),
            "mild" => Ok(Mild),
            "quiet" => Ok(Quiet),
            "bashful" => Ok(Bashful),
            "rash" => Ok(Rash),
            "calm" => Ok(Calm),
            "gentle" => Ok(Gentle),
            "sassy" => Ok(Sassy),
            "careful" => Ok(Careful),
            "quirky" => Ok(Quirky),
            _ => return Err(Error::InvalidInput),
        }
    }
}

impl TryFrom<u8> for Nature {
    type Error = crate::Error;

    fn try_from(n: u8) -> Result<Self, Self::Error> {
        use Nature::*;

        match n {
            0 => Ok(Hardy),
            1 => Ok(Lonely),
            2 => Ok(Brave),
            3 => Ok(Adamant),
            4 => Ok(Naughty),
            5 => Ok(Bold),
            6 => Ok(Docile),
            7 => Ok(Relaxed),
            8 => Ok(Impish),
            9 => Ok(Lax),
            10 => Ok(Timid),
            11 => Ok(Hasty),
            12 => Ok(Serious),
            13 => Ok(Jolly),
            14 => Ok(Naive),
            15 => Ok(Modest),
            16 => Ok(Mild),
            17 => Ok(Quiet),
            18 => Ok(Bashful),
            19 => Ok(Rash),
            20 => Ok(Calm),
            21 => Ok(Gentle),
            22 => Ok(Sassy),
            23 => Ok(Careful),
            24 => Ok(Quirky),
            _ => Err(crate::Error::InvalidInput),
        }
    }
}

impl fmt::Display for Nature {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Nature::*;

        match self {
            Hardy => write!(f, "Hardy"),
            Lonely => write!(f, "Lonely"),
            Brave => write!(f, "Brave"),
            Adamant => write!(f, "Adamant"),
            Naughty => write!(f, "Naughty"),
            Bold => write!(f, "Bold"),
            Docile => write!(f, "Docile"),
            Relaxed => write!(f, "Relaxed"),
            Impish => write!(f, "Impish"),
            Lax => write!(f, "Lax"),
            Timid => write!(f, "Timid"),
            Hasty => write!(f, "Hasty"),
            Serious => write!(f, "Serious"),
            Jolly => write!(f, "Jolly"),
            Naive => write!(f, "Naive"),
            Modest => write!(f, "Modest"),
            Mild => write!(f, "Mild"),
            Quiet => write!(f, "Quiet"),
            Bashful => write!(f, "Bashful"),
            Rash => write!(f, "Rash"),
            Calm => write!(f, "Calm"),
            Gentle => write!(f, "Gentle"),
            Sassy => write!(f, "Sassy"),
            Careful => write!(f, "Careful"),
            Quirky => write!(f, "Quirky"),
        }
    }
}
pub struct NatureIter {
    index: u8,
}

impl Iterator for NatureIter {
    type Item = Nature;

    fn next(&mut self) -> Option<Self::Item> {
        let nature = Nature::try_from(self.index).ok();
        self.index += 1;
        nature
    }
}

impl Nature {
    pub fn iter() -> NatureIter {
        NatureIter { index: 0 }
    }

    pub fn stats_change(&self) -> Option<(Stat, Stat)> {
        let increase = self.increase();
        let decrease = self.decrease();

        (increase != decrease).then_some((increase, decrease))
    }

    pub fn increase(&self) -> Stat {
        use Nature::*;
        use Stat::*;

        match self {
            Hardy | Lonely | Brave | Adamant | Naughty => Attack,
            Bold | Docile | Relaxed | Impish | Lax => Defense,
            Modest | Mild | Quiet | Bashful | Rash => SpecialAttack,
            Calm | Gentle | Sassy | Careful | Quirky => SpecialDefense,
            Timid | Hasty | Serious | Jolly | Naive => Speed,
        }
    }

    pub fn decrease(&self) -> Stat {
        use Nature::*;
        use Stat::*;

        match self {
            Hardy | Bold | Modest | Calm | Timid => Attack,
            Lonely | Docile | Mild | Gentle | Hasty => Defense,
            Adamant | Impish | Bashful | Careful | Jolly => SpecialAttack,
            Naughty | Lax | Rash | Quirky | Naive => SpecialDefense,
            Brave | Relaxed | Quiet | Sassy | Serious => Speed,
        }
    }

    pub fn favorite(&self) -> Flavor {
        use Flavor::*;
        use Nature::*;

        match self {
            Hardy | Lonely | Brave | Adamant | Naughty => Spicy,
            Bold | Docile | Relaxed | Impish | Lax => Sour,
            Modest | Mild | Quiet | Bashful | Rash => Dry,
            Calm | Gentle | Sassy | Careful | Quirky => Bitter,
            Timid | Hasty | Serious | Jolly | Naive => Sweet,
        }
    }

    pub fn disliked(&self) -> Flavor {
        use Flavor::*;
        use Nature::*;

        match self {
            Hardy | Bold | Modest | Calm | Timid => Spicy,
            Lonely | Docile | Mild | Gentle | Hasty => Sour,
            Adamant | Impish | Bashful | Careful | Jolly => Dry,
            Naughty | Lax | Rash | Quirky | Naive => Bitter,
            Brave | Relaxed | Quiet | Sassy | Serious => Sweet,
        }
    }
}
